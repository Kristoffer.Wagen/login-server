import bcrypt
salt = bcrypt.gensalt()


def hash_password(password):
    """Hashes the password for the given user

    Args:
        password (str): password

    Returns:
        bytes: Hashes password in bytes
    """
    return bcrypt.hashpw(password.encode("utf-8"), salt)

def check_password_hash(input_password, password):
    """Compares the hash of input password with hash of password stored in database

    Args:
        input_password (byte): User input of password
        password (byte): Password stored in database

    Returns:
        bool: True if input hash is equal to stored hash
    """
    return bcrypt.checkpw(input_password, password)

def check_password(database, username, password):
    """Compares input of password with stored password

    Args:
        database (Connection): Database for users
        username (str): Username
        password (str): Input password

    Returns:
        bool: True if input password is equal to stored password
    """
    db_password = ""
    for name, ps in database.cursor().execute("SELECT username, password FROM users"):
        if name == username:
            db_password = ps
    if db_password == "":
        return False
    return check_password_hash(password.encode("utf-8"), db_password.encode("utf-8"))