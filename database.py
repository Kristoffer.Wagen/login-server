import sys
import apsw
from apsw import Error
from app import user_exists
from datetime import datetime as dt

def init_database():
    try:
        conn = apsw.Connection('./tiny.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS messages (
            id integer PRIMARY KEY, 
            date TEXT NOT NULL,
            time TEXT NOT NULL,
            sender TEXT NOT NULL,
            receiver TEXT NOT NULL,
            message TEXT NOT NULL);''')
        c.execute('''CREATE TABLE IF NOT EXISTS announcements (
            id integer PRIMARY KEY, 
            author TEXT NOT NULL,
            text TEXT NOT NULL);''')
        c.execute('''CREATE TABLE IF NOT EXISTS users (
            id integer PRIMARY KEY, 
            username TEXT NOT NULL,
            password BLOB NOT NULL,
            token TEXT);''')

        return conn
    except Error as e:
        print(e)
        sys.exit(1)

def search_message(database, username, query):
    message_list = []
    for x in database.cursor().execute(f'SELECT * FROM messages WHERE message GLOB ?', [query]):
        if x[3] == username or x[4] == username or username in x[5]:
            message_list.append(x)

    for x in database.cursor().execute(f'SELECT * FROM messages WHERE receiver GLOB ?', [query]):
        if x[3] == username or x[4] == username or username in x[5]:
            message_list.append(x)

    for x in database.cursor().execute(f'SELECT * FROM messages WHERE sender GLOB ?', [query]):
        if x[3] == username or x[4] == username or username in x[5]:
            message_list.append(x)
    lst = list(set(message_list))
    return lst

def send_message(database, sender, receivers, message):
    now = dt.now()
    date = now.strftime("%d,%m,%y")
    time = now.strftime("%H:%M")

    users = receivers.split(", ")

    users = [x for x in users if user_exists(database, x)]

    if len(users) == 0 or len(message) == 0 or message.count(" ") == len(message) or not user_exists(database, sender):
        return
    
    result = "|".join(users)

    database.cursor().execute(f"INSERT INTO messages (date, time, sender, receiver, message) VALUES ('{date}','{time}','{sender}','{result}','{message}')")
    