# Part 2A - implementation
When implementing the messaging system, the first thing needed to be done was to finish the login system. This was done quite easily by adding a login method and logout method, fixing SQL injection issues, hashing and salting passwords, refactoring the database into it's own module and other methods implemented later.

The issues noted with the given template code is that people can inject code through the poorly designed sql queries. By the current design it allows users to input sql queries when searching for messages and when typing a message in the website's input fields.

The issue was resovled by correctly sanitizing the database inputs.

The database system used is SQLite.

"database.py" contains methods regarding the database used to store user data, these being methods regarding the usage of messaging between users, storing users and more. 

"passwordHandling.py" contains methods regarding hashing of passwords and salting on passwords.

"userHandling.py" contains methods regarding retrieving the user, logging out and in and more.

# Part 2B - Documentation
This messaging design is a very basic implementation of modern communication platforms such as Messenger and Discord and contains these features:

* Form for logging in and button for logging out
* Possibility to send message to registered users
* Functionality to search for messages containing specific information
* Functionality to search for all messages regarding the active user (limited to only reading messages regarding the active user)
* Get timestamps of messages when using the "Search" or "Show all" button

## Instructions for running the program
* Use `flask run` in the terminal in project folder
* Navigate to website hosted at http://localhost:5000
* Login to a registered user (pre registered users are available in the database, example: use "bob" as username and "bananas" as password)
* After successful login, send messages by typing the username of recipient in the "To" form and the message you would like to send in "Message" form
* Search for specific information in message history using "Search" form and clicking "Search!" button
* Show all message history regadring user with "Show all" button
* Log out of user with "Log out" button
* Close server by pressing `Ctrl + C` in the terminal

# 2B - Questions
## Threat model
Threats against the application are users with harmful intentions.

* An attacker can obtain access to a registered user. There are no requirements for passwords, meaning that a person can have "g" as a password. Furthermore, there are no MFA (multi-factor authentication) required upon login.
* Attackers can try to harm the application by initiating DDOS attacks, since there is currently no form of security against these types of attacks

These types of threats damages confidentiality, integrity and availability.

## Main attack vectors
The main issue with the current application is the lack of requiring strong passwords, as of now the passwords are easy to crack and makes the users quite vulnerable. This can lead to a CSRF (Cross-Site Request Forgery) when the user is authenticated. Currently the program redirects the user to a new website when logging in, which is quite common these days. However, the program does not check the URL the program redirects to. Thus creating an opportunity to take advantage of this weakness.

## What should we do (or what have you done) to protect against attacks
As of today there are no form of protection against attacks, besides the coverage of poorly designed SQL queries. It should of course be implemented in the near future, and the ones that should have priority is protection against DDOS attacks, integrity of users (stricter rules for passwords), check URLs that are requested (CSRF protection) and validate and sanitize all variables (Cross-Site Scripting protection).

## What is the access control model
Access control model are ways to authorize access to a system. The main forms of access control are Discretionary Access Control (DAC), Role Based Access Controll (RBAC) and Mandatory Access Control (MAC). These forms are all good models for access control and their usage varies depending upon what type of access control is required. 
* DAC is an access control model that gives a user access to the system based on different rules specified for and by the users. 
* RBAC is an access control model that requires a system administrator to grant access for a user based on their role. Meaning that the user is only granted the access it needs to do its inteded work.
* MAC is an access control model that is based on different settings set by the system administrators and uses a hierarchial approach. When a user needs access to something it is controlled by the system and is only granted access if the setting allows it. This model is considered to be the most strict out of the three.

## How can you know that your security is good enough
There are different ways for a developer to know if the protection they have implemented is good enough. Some of these methods are:

* Application logging
   * Establishing baselines
   * Monitoring policy violations
   * Providing information regarding problems and/or conditions
* Setting up firewalls
* Client software (for instance reporting mechanisms)
* Filters
* Logging events
   * Authorization and authentication failures
   * Data changes
   * Monitoring activity