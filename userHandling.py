from flask import jsonify, request
from flask_login import logout_user
import flask_login

def get_user(database, username):
    """Gets the user from the database

    Args:
        database (Connection): Database
        username (str): Username

    Returns:
        User: Returns user
    """
    for x in database.cursor().execute('SELECT * FROM users WHERE username GLOB ?', [username]):
        if x[0] == username:
            u = User()
            u.id = username
            return u

def user_exists(database, username):
    """Checks if the user is in the database

    Args:
        database (Connection): Database
        username (str): User

    Returns:
        bool: Returns true if the user is in the database
    """
    for x in database.cursor().execute("SELECT username FROM users"):
        if x[0] == username:
            return True
    return False

def logout():
    """Logs the user out

    Returns:
        Literal: Logs the user out and returns a message that they have been logged out
    """
    logout_user()
    return 'LOGGED OUT'

def logged_in_user():
    """Gets the logged in user

    Returns:
        Json: Returns json of the users
    """
    if request.method == "GET":
        return jsonify({"txt" : flask_login.current_user.id})
    
    if request.method == "POST":
        return 'Success', 200
